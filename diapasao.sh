#!/bin/bash

NOTES_DIR=$(dirname $0)/samples
escape_char="$(printf "\u1b")"
default_tuning=("E2" "A2" "D3" "G3" "B3" "E4") # Standard guitar tuning

# https://unix.stackexchange.com/questions/92447/bash-script-to-get-ascii-values-for-alphabet
_chr() {
  [ "$1" -lt 256 ] || return 1
  printf "\\$(printf '%03o' "$1")"
}

_ord() {
  LC_CTYPE=C printf '%d' "'$1"
}

_help() {
  echo "Pass a note [A-G][#b]? as argument. You can also specify the"
  echo "octave by appending it with a number [1-7] (default octave is 3)"
  echo
  echo "Examples: $ diapasao A4"
  echo "          $ diapasao C#2"
  echo "          $ diapasao Gb"
  echo ""
  echo "You can also pass multiple notes to enter a \"tuning\" mode,"
  echo "where you can use arrows to move forwards and backwards between"
  echo "the given notes. Use up and down arrows to change the octave of"
  echo "that note."
  echo
  echo "Example:  $ diapasao E2 A2 D3 G3 B3 E4"
  echo
  echo "If no argument is supplied, a default tuning is used (E2 A2 D3"
  echo "G3 B3 E4), which corresponds to the default tuning of a guitar."
  echo "If a file called \"default_tuning.txt\" is present in the same"
  echo "directory as this script, the tuning described there is used as"
  echo "the default."
  exit 0
}


_diapasao() {
  local note="$1"
  local octave=3

  if [ $# -eq 0 ]; then
    echo "Pass the desired note (A-G#) as an argument."
  elif [ $# -ge 2 ]; then
    echo "Pass only one note (A-G#) as an argument."
  else

    if [ "$note" = "--help" -o "$note" = "-h" ]; then
      _help
    fi

    if [[ $note =~ ^[a-g][#b]?[1-7]?$ ]]; then
      note="$(echo $note | sed -E 's/([a-g])([#b]?[1-7]?$)/\U\1\L\2/')"
    fi

    if ! [[ $note =~ ^[A-G][#b]?[1-7]?$ ]]; then
      echo "Misformed note, should match ^[A-G][#b]?[1-7]?$"
      exit 1
    fi

    # Deduce octave
    if [[ $note =~ ^[A-G][#b]?[1-7] ]]; then
      octave=${note: -1}
      note=${note:0:-1}
    fi

    # If it's a sharp note make it flat
    if [[ $note =~ [A-G]# ]]; then
      note=$(_chr $(( $(_ord ${note:0:1}) + 1 )) )b
    fi
    if [ "$note" = "Hb" ]; then
      note="Ab"
    fi

    paplay $NOTES_DIR/$note$octave.aiff &
  fi
}

# Usage: octave E3 1    -> E4
#        octave E3 -2   -> E1
octave () {
  note="$1"
  add="$2"

  if [[ $note =~ ^[A-G][#b]?[1-7] ]]; then
    octave=${note: -1}
    note=${note:0:-1}
    ((octave += add))
  else
    ((octave = 3 + add))
  fi
  if ((octave > 7)); then octave=7; fi
  if ((octave < 1)); then octave=1; fi
  echo "$note$octave"
  return "$octave"
}

_load_default_tuning() {
  local def_tuning_file='default_tuning.txt'
  if [ ! -f "$def_tuning_file" ]; then
    def_tuning_file="$(dirname ${BASH_SOURCE[0]})/default_tuning.txt"
    if [ ! -f "$def_tuning_file" ]; then
      return
    fi
  fi

  # read words in file into array
  read -r default_tuning < "$def_tuning_file"
  # Use IFS to split words. Do not add quotes to this variable!
  default_tuning=( $default_tuning )
}

# Entry point
diapasao() {
  local i=0
  local octave_d=0
  local last_note=""
  local tuning
  local note

  if [ "$#" -eq 0 ]; then
    _load_default_tuning
    tuning=("${default_tuning[@]}")
    note="${tuning[0]}"
  elif [ "$#" -eq 1 ]; then
    note="$1"
  else
    tuning=("$@")
    note="${tuning[0]}"
  fi

  _diapasao "$note"
  echo "$note"

  if [ ${#tuning[@]} -le 1 ]; then
    return
  fi

  IFS=''
  while read -rsn1 r; do
    if [ "$r" == "$escape_char" ]; then
      read -rsn2 -t 0.001 k1
      read -rsn2 -t 0.001 k2
      read -rsn2 -t 0.001 k3
      r="$k1$k2$k3"
    fi


    case "$r" in
      'n'|'[C')
        ((i = (i + 1) % ${#tuning[@]}))
        note="${tuning[i]}"
        ;;
      'p'|'[D')
        ((i = (i - 1 + ${#tuning[@]}) % ${#tuning[@]}))
        note="${tuning[i]}"
        ;;
      '[A')
        note="$(octave "$note" 1)"
        ;;
      '[B')
        note="$(octave "$note" -1)"
        ;;
      'q'|'')
        break;
        ;;
    esac

    if [ "$last_note" != "$note" ]; then
      last_note="$note"
      echo "$note"
    fi
    _diapasao "$note"

  done
}

diapasao "$@"
