# Diapasao

Simple bash script to play notes on the piano. `./diapasao.sh --help`
yields:

```
Pass a note [A-G][#b]? as argument. You can also specify the
octave by appending it with a number [1-7] (default octave is 3)

Examples: $ diapasao A4
          $ diapasao C#2
          $ diapasao Gb

You can also pass multiple notes to enter a "tuning" mode,
where you can use arrows to move forwards and backwards between
the given notes. Use up and down arrows to change the octave of
that note.

Example:  $ diapasao E2 A2 D3 G3 B3 E4

If no argument is supplied, a default tuning is used (E2 A2 D3
G3 B3 E4), which corresponds to the default tuning of a guitar.
If a file called "default_tuning.txt" is present in the same
directory as this script, the tuning described there is used as
the default.
```

Diapasão means tuning fork in Portuguese.

## Getting samples

The script needs a set of samples whith names corresponding to the notes
they produce. Any folder with audio files named `1A.wav`, `1Bb.wav`,
`1C.wav`, etc, pointed at by the variable `NOTES_DIR` inside
`diapasao.sh` will work. We provide a script to create such a folder,
`download-samples.sh`. The script will download piano samples from the
[University of Iowa Eletronic Music Studios](http://theremin.music.uiowa.edu/),
name them conveniently and cut the silence from their beginning.

## Using as an alias

There is no point to this if it isn't handy at all times. I'd put
something like `alias diapasao=/path/to/diapasao/diapasao.sh` in my
`.bash_aliases` or equivalent file.

## Dependencies

#### `diapasao.sh`

 - `paplay` (`pulseaudio-utils` package for debian based people)

#### `download-samples.sh`

 - `wget`
 - `rename`
 - `sox`

## TODO
 - Remove bashisms
 - Use functions that fulfill a single purpose
