mkdir -p samples
cd samples

for i in {1..7}; do
    for j in {C,Db,D,Eb,E,F,Gb,G,Ab,A,Bb,B}; do
    wget theremin.music.uiowa.edu/sound%20files/MIS/Piano_Other/piano/Piano.mf.${j}${i}.aiff
    done;
done;

rename 's/Piano.mf.//' *
for i in *.aiff; do
  sox $i tmp.aiff silence -l 1 0.01 0.1%; mv tmp.aiff $i
done
